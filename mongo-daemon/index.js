"use strict"
const Thingrator = require("../thingrator")

module.exports = class MongoDbDaemonter extends Thingrator {
  initializing() {
    if (!this.options.composedBy) {
      this._eliosay("MongoDbDaemon the elioWay.")
    }
  }
  writing() {
    this.paths()
    let kebabName = this.kebabName
    this._copy("docker-compose.yaml", "docker-compose.yaml")
    this._copy("Dockerfile", "Dockerfile")
    this._copy("_dockerignore", `.dockerignore`)
  }
}
