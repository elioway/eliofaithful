"use strict"
const Thingrator = require("../thingrator")

module.exports = class Reporator extends Thingrator {
  writing() {
    this.paths()
    let kebabName = this.kebabName
    this._write("_branciaos", ".branciaos")
    this._copy("_editorconfig", ".editorconfig")
    this._copy("_gitignore", ".gitignore")
    this._copy("LICENSE")
    this._write("README.md")
    this._write("artwork/_artwork.yaml", "artwork/artwork.yaml")
    this._copy("artwork/icon.png")
    this._copy("artwork/splash.jpg")
    this._write("doc/credits.md")
    this._write("doc/index.md")
    this._write("doc/installing.md")
    this._write("doc/quickstart.md")
    this._write("doc/prerequisites.md")
  }
  end() {
    this._eliosay("It is done!")
  }
}
