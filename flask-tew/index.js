"use strict"
const Thingrator = require("../thingrator")

module.exports = class Flaskator extends Thingrator {
  initializing() {
    if (!this.options.composedBy) {
      this._eliosay("Flask the elioWay.")
    }
    this.composeWith("thing:python", {
      arguments: [this.elioName, this.elioGroup],
      composedBy: "Flaskator",
    })
  }
  writing() {
    this.paths()
    let snakeName = this.snakeName
    this._write("README.md")
    this._write("run.py")
    this._write("setup.py")
    this._write("wsgi.py")
    this._write("doc/installing.md")
    this._write("installers/nameOf", `installers/${snakeName}`)
    this._write("installers/nameOf.service", `installers/${snakeName}.service`)
    this._write("nameOf/app.py", `${snakeName}/app.py`)
    this._write("nameOf/config.py", `${snakeName}/config.py`)
    this._write("nameOf/jinja_filters.py", `${snakeName}/jinja_filters.py`)
    this._write("nameOf/utils.py", `${snakeName}/utils.py`)
    this._write("nameOf/views.py", `${snakeName}/views.py`)
    this._copy(
      "nameOf/static/manifest.json",
      `${snakeName}/static/manifest.json`
    )
    this._copy(
      "nameOf/static/css/nameOf.min.css",
      `${snakeName}/static/css/${snakeName}.min.css`
    )
    this._copy(
      "nameOf/static/css/nameOf.min.css.map",
      `${snakeName}/static/css/${snakeName}.min.css.map`
    )
    this._write(
      `nameOf/templates/base.html`,
      `${snakeName}/templates/base.html`
    )
    this._write(
      `nameOf/templates/index.html`,
      `${snakeName}/templates/index.html`
    )
    this._write(
      `nameOf/templates/nameOf.html`,
      `${snakeName}/templates/${snakeName}.html`
    )
    this._copy("requirements/base.txt")
    this._copy("requirements/local.txt")
  }
}
