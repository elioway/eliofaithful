# Installing <%= elioName %>

## Prerequisites

- [elioWay Prerequisites](https://elioway.gitlab.io/installing.html)
- [<%= elioGroup %> Prerequisites](https://elioway.gitlab.io/<%= elioGroup %>/installing.html)

## Installing

```shell
virtualenv --python=python3 venv-<%= snakeName %>
source venv-<%= snakeName %>/bin/activate
# or
source venv-<%= snakeName %>/bin/activate.fish
pip install -r requirements/local.txt
./run.py
```

- USER: <%= snakeName %>
- PASS: letmein

Navigate to [<%= elioName %>](http://localhost:5000)

### Configuration

Create a `<%= snakeName %>.cfg` file and put it somewhere outside of this repo. You could put it somewhere in your home directory.

```
# /home/<user>/<%= snakeName %>/<%= snakeName %>.cgf
<%= UPPERNAME %>_USER="<username>"
<%= UPPERNAME %>_PASS="<password>"
```

- Create a `.env` or `.private` file. It needs just one setting - `CONFIG_PATH` - which will point to the config file described above.

```
CONFIG_PATH=/home/<user>/<%= snakeName %>/<%= snakeName %>.cfg
```

### Create a Gunicorn Service

- Edit the paths in the file `installers/<%= snakeName %>.service` relative to the **<%= elioName %>** repo you cloned, e.g.:

  - `WorkingDirectory=/home/<user>/<%= snakeName %>`
  - `Environment="PATH=/home/<user>/<%= snakeName %>/venv-<%= snakeName %>/bin"`
  - `EnvironmentFile=/home/<user>/<%= snakeName %>/.private`
  - `ExecStart=/home/<user>/<%= snakeName %>/venv-<%= snakeName %>/bin/gunicorn --workers 3 --bind unix:<%= snakeName %>.sock -m 007 wsgi:app`

- Run the following commands.

```shell
# sudo rm /etc/systemd/system/installers/<%= snakeName %>.service
sudo cp installers/<%= snakeName %>.service /etc/systemd/system/

sudo systemctl start <%= snakeName %>
sudo systemctl enable <%= snakeName %>
```

Status check.

```shell
sudo systemctl status <%= snakeName %>
```

Issues:

```shell
sudo systemctl daemon-reload
sudo systemctl start <%= snakeName %>
```

### Nginx setup

- Find out what your machine's IP address is using `ifconfig`. It will usually start `192.168.`, otherwise you've set things up differently on this machine and you'll probably know what it is without my help.

- Edit the paths in the file `installers/<%= snakeName %>`, e.g.:

  - `server_name <IP_ADDRESS>;`
  - `proxy_pass http://unix://home/<user>/<%= snakeName %>/<%= snakeName %>.sock;`

- Run the following commands.

```shell
sudo cp installers/<%= snakeName %> /etc/nginx/sites-available/<%= snakeName %>
sudo ln -s /etc/nginx/sites-available/<%= snakeName %> /etc/nginx/sites-enabled
sudo systemctl restart nginx
sudo ufw allow 'Nginx Full'
```

Status check.

```shell
sudo nginx -t
```

Issues:

- Apache home page showing? Remove the default site.

```
sudo rm /etc/nginx/sites-enabled/default
```

## Development

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/<%= elioGroup %>.git
cd <%= elioGroup %>
git clone https://gitlab.com/<%= elioGroup %>/<%= elioName %>.git
```
