![](https://elioway.gitlab.io/<%= elioGroup %>/<%= elioName %>/elio-<%= kebabName %>-logo.png)

> <%= CamelName %>, **the elioWay**

# <%= elioName %>

A Flask app built **the elioWay**.

- [<%= elioName %> Documentation](https://elioway.gitlab.io/<%= elioGroup %>/<%= elioName %>/)

  ## Seeing is believing

```shell
virtualenv --python=python3 venv-<%= snakeName %>
source venv-<%= snakeName %>/bin/activate
# or
source venv-<%= snakeName %>/bin/activate.fish
pip install -r requirements/local.txt
./run.py
```

- USER: <%= snakeName %>
- PASS: letmein

Navigate to [<%= elioName %>](http://localhost:5000)

## Nutshell

```
./run.py
```

- [<%= elioGroup %> Quickstart](https://elioway.gitlab.io/<%= elioGroup %>/quickstart.html)
- [<%= elioName %> Quickstart](https://elioway.gitlab.io/<%= elioGroup %>/<%= elioName %>/quickstart.html)

# Credits

- [<%= elioName %> Credits](https://elioway.gitlab.io/<%= elioGroup %>/<%= elioName %>/credits.html)

## License

[MIT](LICENSE) [Tim Bushell](mailto:tcbushell@gmail.com)

![](https://elioway.gitlab.io/<%= elioGroup %>/<%= elioName %>/apple-touch-icon.png)
