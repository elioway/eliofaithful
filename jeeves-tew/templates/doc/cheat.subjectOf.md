# Cheat <%= cheatSubject %>

```
<%= cheatSubject %> --help
```

## Subhead

```
# Subhead cheats
<%= cheatSubject %> subhead --help
```

### More

```
#### more
<%= cheatSubject %> more --deploy-things
```
